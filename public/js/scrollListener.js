const coverImg = document.getElementById('cover-img');

function debounce(callback, wait) {
   let timerId;
   return (...args) => {
      clearTimeout(timerId);
      timerId = setTimeout(() => {
         callback(...args);
      }, wait)
   }
}



document.addEventListener('scroll', debounce(() => {
   if (scrollY < 1000) {
      coverImg.style.filter = `blur(${scrollY * 0.02}px)`;
   };
 }, 10));